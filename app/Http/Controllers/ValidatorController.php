<?php
namespace App\Http\Controllers;

use \Datetime;

class ValidatorController extends Controller
{
    public function isValid($code)
    {
        $valid = false;

        $codeArr = array_map('intval', str_split($code));

        if (count($codeArr) === 11) {
            if ($codeArr[0] >= 1 && $codeArr[0] < 7) {
                if (checkdate($codeArr[3] . $codeArr[4], $codeArr[5] . $codeArr[6],
                    $codeArr[1] . $codeArr[2]) || array_sum(array_slice($codeArr, 3, 4)) === 0) {

                    $sum = $codeArr[0] * 1 + $codeArr[1] * 2 + $codeArr[2] * 3 +
                        $codeArr[3] * 4 + $codeArr[4] * 5 + $codeArr[5] * 6 + $codeArr[6] * 7 +
                        $codeArr[7] * 8 + $codeArr[8] * 9 + $codeArr[9] * 1;

                    $controlNumber = $sum % 11;

                    if ($controlNumber === 10) {
                        $sum = $codeArr[0] * 3 + $codeArr[1] * 4 + $codeArr[2] * 5 +
                            $codeArr[3] * 6 + $codeArr[4] * 7 + $codeArr[5] * 8 + $codeArr[6] * 9 +
                            $codeArr[7] * 1 + $codeArr[8] * 2 + $codeArr[9] * 3;

                        $controlNumber = $sum % 11;

                        if ($controlNumber === $codeArr[10]) {
                            $valid = true;
                        } elseif ($controlNumber === 10 && $codeArr[10] === 0) {
                            $valid = true;
                        }

                    } elseif ($controlNumber === $codeArr[10]) {
                        $valid = true;
                    }
                }
            } elseif ($codeArr[0] === 9) {
                $valid = true;
            }
        }

        return response()->json($valid);
    }

    public function codeGenerator($date, $gender)
    {
        if (!$this->validateDate($date)) {
            return response()->json("Bad Date!");
        }
        if (!($gender > 0 && $gender < 7 || $gender == 9)) {
            return response()->json("Bad Gender!");
        }

        $dateArr = array_map('intval', str_split($date));

        $firstPart = $gender . $dateArr[2] . $dateArr[3] .
            $dateArr[5] . $dateArr[6] . $dateArr[8] . $dateArr[9];

        $codes = [];

        for ($i = 0; $i <= 999; $i++) {
            $str_padded = str_pad($i, 3, "0", STR_PAD_LEFT);
            $code = $firstPart . $str_padded;
            for ($j = 0; $j <= 9; $j++) {
                $fullCode = $code . $j;
                if ($this->isValid($fullCode)->original) {
                    array_push($codes, $fullCode);
                }
            }
        }

        return response()->json(["codes"=>$codes]);
    }

    protected function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);

        return $d && $d->format($format) == $date;
    }

}
